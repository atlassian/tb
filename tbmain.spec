# -*- mode: python -*-

block_cipher = None

import cement
import dateutil
import pkgutil
import blessings
import watchdog
import platform
import importlib
from PyInstaller.utils.hooks import copy_metadata


def find_abs_modules_by_module(module):
    try:
        return find_abs_modules(module.__name__, module.__path__[0])
    except AttributeError:
        return [module.__name__]

def find_abs_modules(name, path):
    for root,dirname,filename in os.walk(path):
        pth_build=""
        if os.path.isfile(root+"/__init__.py"):
            for i in filename:
                if i != "__init__.py" and i != "__init__.pyc":
                    if i.split('.')[1] == "py":
                        slot = list(set(root.split('/')) -set(path.split('/')))
                        if slot:
                            pth_build = slot[0]
                            del slot[0]
                            for j in slot:
                                pth_build = pth_build+"."+j
                        if not pth_build:
                            yield name
                        else:
                            print("module: {}".format(name + "." + pth_build +"."+ i.split('.')[0]))
                            yield name + "." + pth_build +"."+ i.split('.')[0]

def hiddenimports(*modules):
    result = []
    for module in modules:
        print(f"importing: {module}")
        imp = importlib.import_module(module)
        result += [x for x in find_abs_modules_by_module(imp)]
    return result

if 'Linux' == platform.system():
    platform_hiddenimports = hiddenimports('secretstorage')
elif 'Darwin' == platform.system():
    platform_hiddenimports = hiddenimports('AppKit',
        'FSEvents', 
        'objc',
        'AVFoundation',
        'AVKit',
        'Accounts',
        'AddressBook', 
        'AppleScriptKit',
        'AppleScriptObjC',
        'ApplicationServices',
        'Automator', 
        'CFNetwork',
        'CFOpenDirectory',
        'CalendarStore',
        'CloudKit', 
        'Cocoa',
        'Collaboration',
        'ColorSync',
        'Contacts', 
        'ContactsUI',
        'CoreAudio',
        'CoreAudioKit',
        'CoreBluetooth', 
        'CoreData',
        'CoreFoundation',
        'CoreLocation',
        'CoreML', 
        'CoreMedia',
        'CoreMediaIO',
        'CoreServices',
        'CoreSpotlight',
        'CoreText',
        'CoreWLAN',
        'CryptoTokenKit',
        'DictionaryServices',
        'DiscRecording',
        'DiscRecordingUI',
        'DiskArbitration',
        'EventKit',
        'ExceptionHandling',
        'ExternalAccessory',
        'FinderSync',
        'GameCenter',
        'GameController',
        'GameKit',
        'GameplayKit',
        'HIServices',
        'IMServicePlugIn',
        'IOSurface',
        'ImageCaptureCore',
        'InputMethodKit',
        'InstallerPlugins',
        'InstantMessage',
        'Intents',
        'JavaScriptCore',
        'LatentSemanticMapping',
        'LaunchServices',
        'LocalAuthentication',
        'MapKit',
        'MediaAccessibility',
        'MediaLibrary',
        'MediaPlayer',
        'MediaToolbox',
        'ModelIO',
        'MultipeerConnectivity',
        'NetFS',
        'NetworkExtension',
        'NotificationCenter',
        'OSAKit',
        'OpenDirectory',
        'Photos',
        'PhotosUI',
        'PreferencePanes',
        'PubSub',
        'QTKit',
        'Quartz',
        'SafariServices',
        'SceneKit',
        'ScreenSaver',
        'ScriptingBridge',
        'SearchKit',
        'Security',
        'SecurityFoundation',
        'SecurityInterface',
        'ServiceManagement',
        'Social',
        'SpriteKit',
        'SyncServices',
        'SystemConfiguration',
        'VideoToolbox',
        'Vision',
        'WebKit',
        'Foundation',
        'libdispatch',
        'libfuturize',
        'libpasteurize')
else:
    platform_hiddenimports = []


a = Analysis(['tbmain.py'],
             pathex=['src'],
             binaries=[],
              datas=[('plugins.d', 'plugins.d'),
                     ('config', 'config'),
                     ('plugins', 'plugins')] + \
                     copy_metadata('git-url-parse'),
             hiddenimports=['tb'] +
             [x for x in find_abs_modules('tb', './src/tb')] +
             hiddenimports('dateutil', 'blessings', 'watchdog', 'cement', 'tabulate', 'ruamel.yaml', 'requests_oauthlib',
                'giturlparse', 'keyring', 'fileinput') +
             platform_hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='tb',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
