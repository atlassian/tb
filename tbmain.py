from tb.first_time import ensure_user_tb_config

try:
    ensure_user_tb_config()

    from tb.main import main
    main()
except KeyboardInterrupt:
    pass
