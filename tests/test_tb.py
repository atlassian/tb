from tb.tb import TbTest


def test_tb():
    # test tb without any subcommands or arguments
    with TbTest() as app:
        app.run()
        assert app.exit_code == 0


def test_tb_debug():
    # test that debug mode is functional
    argv = ['--debug']
    with TbTest(argv=argv) as app:
        app.run()
        assert app.debug is True

