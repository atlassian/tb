import os

from cement import TestApp
from git import Repo

from tb import Tb
from tb.tb import set_default_env

import pytest
from cement import fs


@pytest.fixture(scope="function")
def tmp(request):
    """
    Create a `tmp` object that generates a unique temporary directory, and file
    for each test function that requires it.
    """
    t = fs.Tmp()
    yield t
    t.remove()


@pytest.fixture(scope="function")
def git_repo(request, tmp):
    orig_git_dir = os.path.join(tmp.dir, 'src', 'blah')
    os.makedirs(orig_git_dir)
    with open(os.path.join(orig_git_dir, 'foo.txt'), 'w') as f:
        f.write("hi")

    repo = Repo.init(orig_git_dir)
    repo.index.add(['foo.txt'])
    repo.index.commit("Init commit")

    return f"file://{os.path.abspath(orig_git_dir)}"


def test_org_repo_clone(tmp, git_repo):
    class TbTest(Tb, TestApp):
        """A sub-class of Tb that is better suited for testing."""

        class Meta:
            label = 'tb'
            config_defaults = dict(
                tb=dict(
                    src_dir=tmp.dir,
                    org_repo=git_repo,
                    teams=[]
                ),
            )
            plugins = ['repo']
            plugin_dirs = [os.path.join(os.path.dirname(os.path.dirname(__file__)), 'plugins')]
            hooks = [
                ('post_argument_parsing', set_default_env),
            ]

    with TbTest(argv=['repo']) as app:
        app.run()
        assert app.exit_code == 0

    assert os.path.exists(os.path.join(tmp.dir, 'blah', 'foo.txt'))
