#!/usr/bin/env sh

set -x

python -m venv _venv
. _venv/bin/activate
pip install -r ci/requirements.txt
git checkout -b release_branch

VERSION=`python ci/changelog.py`
git add CHANGES.md
bumpversion release --allow-dirty
ci/release-downloads.sh "latest-linux-amd64"
bumpversion --no-tag patch
git checkout master
git pull origin master
git merge --no-edit release_branch
git branch -d release_branch
git push origin master --tags
deactivate
rm -rf _venv
#ci/update-tap.sh ${VERSION}

