#!/usr/bin/env sh

set -x
set -e

if [ -z "$BB_AUTH_STRING" ]; then
  echo "No Bitbucket credentials found, set them in username:password form in the BB_AUTH_STRING env variable"
  exit 1
fi

# This assumes it is running in a virtualenv already
pip install -r ci/requirements.txt
pip install -r requirements.txt

VERSION=$1
FILE=tb-${VERSION}.tar.gz

pyinstaller tbmain.spec --onefile
tar cfvz $FILE -C dist tb

# turn off command echoing so we don't show the password in your terminal
set +x
echo "Uploading $FILE to Bitbucket https://api.bitbucket.org/2.0/repositories/atlassian/tb/downloads"
curl --fail -X POST -u "${BB_AUTH_STRING}" "https://api.bitbucket.org/2.0/repositories/atlassian/tb/downloads" --form files=@"$FILE"



